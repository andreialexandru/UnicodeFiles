#include "stdafx.h"
#include "UnicodeFileConverter.h"
#include "BufferEncoding.h"
#include "Utils.h"

using namespace UnicodeFiles;

UnicodeFileConverter::UnicodeFileConverter()
	: UnicodeFile()
{

}

UnicodeFileConverter::UnicodeFileConverter(std::wstring aFilePath, unsigned int aFlags)
	: UnicodeFile(aFilePath, aFlags), mFlags(aFlags), mFilePath(aFilePath)
{

}

void UnicodeFileConverter::Reset(std::wstring aFilePath, unsigned int aFlags)
{
	UnicodeFile::Reset(aFilePath, aFlags);
	mFlags = aFlags;
	mFilePath = aFilePath;
}

std::wstring UnicodeFileConverter::ReadFileStr(unsigned int aChunkSize) const
{
	return Utils::Buff_Wstring(BufferEncoding::Convert(ReadFile(aChunkSize), CharEncoding::Utf16_LE_NoBom));
}

void UnicodeFileConverter::WriteFileStr(const std::wstring& aContent, CharEncoding aEncoding, bool aResetPos)
{
	if ((mFlags & UF_APPEND) && BufferEncoding::HasBom(aEncoding) && GetFileSize())
	{
		switch (aEncoding)
		{
		case CharEncoding::Utf8:
			aEncoding = CharEncoding::Utf8_NoBom;
		case CharEncoding::Utf16_LE:
			aEncoding = CharEncoding::Utf16_LE_NoBom;
		case CharEncoding::Utf16_BE:
			aEncoding = CharEncoding::Utf16_BE_NoBom;
		}
	}

	WriteFile(BufferEncoding::Convert(Utils::Wstring_Buff(aContent), aEncoding), aResetPos);
}

void UnicodeFileConverter::Convert(CharEncoding aEncoding)
{
	//open the file for reading, read and convert the content
	UnicodeFile::Reset(mFilePath, UF_READ);
	auto buff = BufferEncoding::Convert(ReadFile(), aEncoding);

	//then truncate it, and write the converted content
	UnicodeFile::Reset(mFilePath, UF_WRITE | UF_TRUNCATE);
	WriteFile(buff);

	UnicodeFile::Reset(mFilePath, mFlags);
}