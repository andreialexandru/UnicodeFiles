#pragma once
#include "ScopedFileHandle.hpp"

/**@file*/

namespace UnicodeFiles
{
	/**
	*Enumeration of flags used for controlling how the file is opened.
	*/
	enum UF_Flag
	{
		UF_DEFAULT = 0,			/**<Default Value.*/

		UF_READ = 1,			/**<Open file for reading.*/
		UF_WRITE = 2,			/**<Open file for writing.*/

		UF_CREATE = 4,			/**<Create file if it doesn't exists.*/
		
		UF_APPEND = 8,			/**<Append to file (write at the end of the file).*/
		UF_TRUNCATE = 16,		/**<Truncate the file when opening (delete the content).*/
		UF_NO_BUFFERING = 32	/**<Flush after each writing, sending the content to disk. Might be more inefficient.*/
	};

	/**
	*Class for reading and writing a file.
	*/
	class UnicodeFile
	{
	public:
		/**
		*Default constructor, initialize members with default values.
		*/
		UnicodeFile();

		/**
		*Open a file with certain flags.
		*
		*@param aFilePath The file we want to open.
		*@param aFlags [default = UF_DEFAULT] The flags we use to open the file. If not sent, the file is open for reading and writing with no other flags.
		*@see UF_Flag
		*/
		UnicodeFile(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT);

		/**
		*Flushes the file and closes it.
		*/
		virtual ~UnicodeFile();
		
		/**
		*(Re)Open a file with certain flags.
		*
		*@param aFilePath The file we want to open.
		*@param aFlags [default = UF_DEFAULT] The flags we use to open the file. If not sent, the file is open for reading and writing with no other flags.
		*@see UF_Flag
		*/
		virtual void Reset(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT);

		/**
		*Reads a file into a buffer (vector of unsigned chars).
		*
		*@param aChunkSize [default = 0] The number of bytes we want to read from the file. If not set, all the file is read.
		*@return The file content.
		*/
		std::vector<unsigned char> ReadFile(unsigned int aChunkSize = 0) const;

		/**
		*Writes a buffer into a file.
		*
		*@param aBuffer The buffer to be written in the file.
		*@param aResetPos [default = true] Reset the cursor's position to the beginning of the file after writing.
		*/
		void WriteFile(const std::vector<unsigned char>& aBuffer, bool aResetPos = true);

		/**
		*Send the file content to the disk.
		*/
		void Flush();

	protected:
		/**
		*Get the file size.
		*/
		int GetFileSize() const;

	private:
		ScopedFileHandle mFileHandle;

		bool mNoBuffering;
		bool mAppend;
		bool mWrite;

	};
}