#include "stdafx.h"
#include "UnicodeFile.h"
#include "UnicodeFileConverter.h"
#include "BufferEncoding.h"
#include "Exception.h"

using namespace UnicodeFiles;

//convert from every encoding to every encoding and back, then compare if equal. Ansi is an exception because it can be lossy.
void TestConversions(const std::vector<unsigned char>& buff)
{
	for (int i = 0; i < static_cast<int>(CharEncoding::Unknown_Encoding); i++)
	{
		auto buffConvert1 = BufferEncoding::Convert(buff, static_cast<CharEncoding>(i));
		for (int j = 0; j < static_cast<int>(CharEncoding::Unknown_Encoding); j++)
		{
			auto tempBuff = BufferEncoding::Convert(buffConvert1, static_cast<CharEncoding>(j));

			auto buffConvert2 = BufferEncoding::Convert(tempBuff, static_cast<CharEncoding>(i));

			if (buffConvert2 != buffConvert1)
				auto a=0;
			assert(buffConvert1 == buffConvert2 || j == static_cast<int>(CharEncoding::Ansi)); //utf8/16 -> ansi might be lossy
		}
	}
}

int main()
{
	UnicodeFile f;
	UnicodeFileConverter fc(L"abcd.efgh");
	try
	{
		f.Reset(L"abcd.efg", UF_READ);
		TestConversions(f.ReadFile());

		fc.Convert(CharEncoding::Utf16_BE);
		fc.Convert(CharEncoding::Utf8);
		fc.Convert(CharEncoding::Utf16_LE);
		auto str = fc.ReadFileStr();
	}
	catch (Exception e)
	{
		OutputDebugStringW(e.Str().c_str());
	}

	return 0;
}

