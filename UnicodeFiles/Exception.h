#pragma once
#include <memory>

/**@file*/

/**
*Wrapper over an error code and a message.
*
*The Exception class holds an error code and an error message.
*It is mainly used together with the Maybe class for error handling.
*@see CustomErrorCode
*@see Maybe
*/
class Exception
{
public:
	/**
	*Class constructor.
	*
	*An explicit constructor used for windows error codes.
	*@param pErrorCode Windows error code.
	*@param pErrorMessage Error message.
	*@param pInnerException [optional][default = nullptr] Another exception that is kept by this object; usually a lower level exception.
	*
	*Example Usage:
	*@code
	*	Exception(ERROR_INVALID_HANDLE, L"The handle is invalid!");
	*@endcode
	*
	*@see explicit Exception(CustomErrorCode pErrorCode, std::wstring pErrorMessage = L"")
	*/
	explicit Exception(unsigned int pErrorCode, std::wstring pErrorMessage, std::unique_ptr<Exception> pInnerException = nullptr);

	/**
	*Class move constructor.
	*
	*@param pOther the object to be moved.
	*/
	Exception(Exception&& pOther);

	/**
	*Class copy constructor.
	*
	*@param pOther the object to be copy.
	*/
	Exception(const Exception& pOther);

	/**
	*Assignment move operator.
	*
	*@param pOther the object to be moved.
	*/
	Exception& operator=(Exception&& pOther);

	/**
	*Assignment move operator.
	*
	*@param pOther the object to be copy.
	*/
	Exception& operator=(const Exception& pOther);

	/**
	*Get the error code.
	*
	*@return The error code.
	*/
	unsigned int GetCode() const;

	/**
	*Get the error message.
	*
	*@return The error message.
	*/
	const std::wstring& GetMessage() const;

	/**
	*Get the inner exception.
	*
	*@return The error message.
	*/
	const Exception* const GetInnerException() const;

	/**
	*Get the error description.
	*
	*Returns a concatenated string of type "[ErrorCode]ErrorMessage".
	*@return The error description.
	*/
	std::wstring Str() const;

private:
	unsigned int mErrorCode;
	std::wstring mErrorMessage;
	std::unique_ptr<Exception> mInnerException;
};
