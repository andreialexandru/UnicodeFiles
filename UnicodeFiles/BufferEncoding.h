#pragma once

/**@file*/

namespace UnicodeFiles
{
	/**
	*Enumeration that contains the encodings supported. Names are self explanatory.
	*/
	enum class CharEncoding
	{
		Ansi, Utf8, Utf8_NoBom, Utf16_LE, Utf16_LE_NoBom, Utf16_BE, Utf16_BE_NoBom, Unknown_Encoding
	};

	/**
	*Conversion Class.
	*
	*This class has several static functions for converting buffers from one encoding to another.
	*/
	class BufferEncoding
	{
	public:
		/**
		*Deleted constructor, so the class is not instantiable.
		*/
		BufferEncoding() = delete;

		/**
		*Get the buffer's encoding.
		*
		*@param aBuffer Buffer for which we want to find the encoding.
		*@return The buffer's encoding.
		*/
		static CharEncoding GetEncoding(const std::vector<unsigned char>& aBuffer);

		/**
		*Convert a buffer from it's current encoding to the specified one.
		*
		*@param aBuffer Buffer to convert.
		*@param aEncoding Encoding to convert the buffer to.
		*@return The converted buffer.
		*/
		static std::vector<unsigned char> Convert(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);

		/**
		*Check if a buffer has bom or not.
		*
		*@param aBuffer Buffer for which we want to find if it has bom or not.
		*@retval true aBuffer has bom.
		*@retval false aBuffer doesn't have bom.
		*/
		static bool HasBom(const std::vector<unsigned char>& aBuffer);

		/**
		*Check if an encoding has bom or not.
		*
		*@param aEncoding The for which we want to find if it has bom or not.
		*@retval true aEncoding is one of: Utf8, Utf8_LE, Utf8_BE.
		*@retval false aEncoding is one of: Utf8_NoBom, Utf8_LE_NoBom, Utf8_BE_NoBom.
		*/
		static bool HasBom(CharEncoding aEncoding);

		/**
		*Strip a buffer's bom.
		*
		*@param aBuffer The buffer we want to strip bom from.
		*@return The buffer without bom.
		*@remark If the input buffer doesn't have bom, the function will return the buffer unchanged.
		*/
		static std::vector<unsigned char> StripBom(const std::vector<unsigned char>& aBuffer);

		/**
		*Add a bom to a buffer.
		*
		*@param aBuffer The buffer we want to add the bom to.
		*@param aEncoding The encoding we want the bom for.
		*@return The buffer with bom.
		*@remark If the input buffer already has bom or the encoding doesn't have bom, the function will return the buffer unchanged.
		*/
		static std::vector<unsigned char> AddBom(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);

		/**
		*Get the buffer's bom.
		*
		*@param aBuffer The buffer from which we want to extract the bom.
		*@return The buffer's bom.
		*@remark If the input buffer doesn't have bom, the function will return an empty vector.
		*/
		static std::vector<unsigned char> GetBom(const std::vector<unsigned char>& aBuffer);

	private:
		static std::vector<unsigned char> ConvertFromAnsi(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);
		static std::vector<unsigned char> ConvertFromUtf8(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);
		static std::vector<unsigned char> ConvertFromUtf16LE(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);
		static std::vector<unsigned char> ConvertFromUtf16BE(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding);
		

		static std::vector<unsigned char> Utf8_Ansi(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Ansi_Utf8(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> Utf16LE_Ansi(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Ansi_Utf16LE(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> Utf16BE_Ansi(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Ansi_Utf16BE(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> Utf16LE_Utf8(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Utf8_Utf16LE(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> Utf16BE_Utf8(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Utf8_Utf16BE(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> Utf16BE_Utf16LE(const std::vector<unsigned char>& aBuffer);
		static std::vector<unsigned char> Utf16LE_Utf16BE(const std::vector<unsigned char>& aBuffer);

		static std::vector<unsigned char> ChangeEndianness(const std::vector<unsigned char>& aBuffer);

		static const std::vector<unsigned char> kUtf8_BOM;
		static const std::vector<unsigned char> kUtf16LE_BOM;
		static const std::vector<unsigned char> kUtf16BE_BOM;
	};
}
