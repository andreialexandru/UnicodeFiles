#include "stdafx.h"

#include "Utils.h"
#include "Exception.h"

using namespace UnicodeFiles;

Exception Utils::CheckLastError(bool aThrow)
{
	//Get the error message, if any.
	unsigned long errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return Exception(NO_ERROR, L"");

	LPWSTR pBuffer = nullptr;
	size_t size = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
								nullptr, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&pBuffer, 0, nullptr);

	std::wstring message(pBuffer, size);

	//Free the buffer.
	LocalFree(pBuffer);

	if (aThrow)
		throw Exception(errorMessageID, message);

	return Exception(errorMessageID, message);
}

std::wstring Utils::Buff_Wstring(const std::vector<unsigned char>& aBuff)
{
	return std::wstring(reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuff.data())),
						reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuff.data() + aBuff.size())));
}

std::string Utils::Buff_String(const std::vector<unsigned char>& aBuff)
{
	return std::string(reinterpret_cast<char*>(const_cast<unsigned char*>(aBuff.data())),
						reinterpret_cast<char*>(const_cast<unsigned char*>(aBuff.data() + aBuff.size())));
}

std::vector<unsigned char> Utils::Wstring_Buff(const std::wstring& aStr)
{
	return std::vector<unsigned char>(reinterpret_cast<unsigned char*>(const_cast<wchar_t*>(aStr.data())),
									reinterpret_cast<unsigned char*>(const_cast<wchar_t*>(aStr.data() + aStr.size())));
}

std::vector<unsigned char> Utils::Buff_String(const std::string& aStr)
{
	return std::vector<unsigned char>(reinterpret_cast<unsigned char*>(const_cast<char*>(aStr.data())),
									  reinterpret_cast<unsigned char*>(const_cast<char*>(aStr.data() + aStr.size())));
}
