#pragma once
#include "UnicodeFile.h"

/**@file*/

namespace UnicodeFiles
{
	enum class CharEncoding;

	/**
	*Class for reading and writing a file using wstrings.
	*
	*@see UnicodeFile
	*/
	class UnicodeFileConverter : public UnicodeFile
	{
	public:
		/**
		*Default constructor, initialize members with default values.
		*
		*@see UnicodeFile()
		*/
		UnicodeFileConverter();

		/**
		*Open a file with certain flags.
		*
		*@param aFilePath The file we want to open.
		*@param aFlags [default = UF_DEFAULT] The flags we use to open the file. If not sent, the file is open for reading and writing with no other flags.
		*@see UF_Flag
		*@see UnicodeFile(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT)
		*/
		UnicodeFileConverter(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT);

		/**
		*Open a file with certain flags.
		*
		*@param aFilePath The file we want to open.
		*@param aFlags [default = UF_DEFAULT] The flags we use to open the file. If not sent, the file is open for reading and writing with no other flags.
		*@see UF_Flag
		*@see UnicodeFile::Reset(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT)
		*/
		virtual void Reset(std::wstring aFilePath, unsigned int aFlags = UF_DEFAULT) override;

		/**
		*Reads a file into a wstring.
		*
		*@param aChunkSize [default = 0] The number of bytes we want to read from the file. If not set, all the file is read.
		*@return The file content.
		*@remark wstring is encoded as Utf8_LE, so the file content is read and converted into Utf8_LE.
		*/
		std::wstring ReadFileStr(unsigned int aChunkSize = 0) const;

		/**
		*Writes a wstring into a file.
		*
		*@param aContent The wstring to be written in the file.
		*@param aEncoding The encoding used when writing in the file.
		*@param aResetPos [default = true] Reset the cursor's position to the beginning of the file after writing.
		*@remark wstring is encoded as Utf8_LE, so before writing into the file in aEncoding format, the string is converted.
		*/
		void WriteFileStr(const std::wstring& aContent, CharEncoding aEncoding, bool aResetPos = true);
		
		/**
		*Convert a file into another format.
		*
		*@param aEncoding The encoding into which the file is converted.
		*/
		void Convert(CharEncoding aEncoding);

	private:
		unsigned int mFlags;
		std::wstring mFilePath;
	};
}