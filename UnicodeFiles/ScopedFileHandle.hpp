#pragma once
#include "ScopedHandle.hpp"

namespace UnicodeFiles
{
	typedef ScopedHandle<HANDLE> ScopedFileHandle;

	template<>
	inline HANDLE ScopedHandle<HANDLE>::GetDefaultValue()
	{
		return INVALID_HANDLE_VALUE;
	}

	template<>
	inline void ScopedHandle<HANDLE>::Close()
	{
		if (mObject != GetDefaultValue())
		{
			CloseHandle(mObject);
		}
	}

	template<>
	inline void ScopedHandle<HANDLE>::Set(const HANDLE& aObject)
	{
		if (mObject != aObject)
		{
			mObject = aObject;
		}
	}

	template<>
	inline void ScopedHandle<HANDLE>::Reset(const HANDLE& aObject)
	{
		Close();
		Set(aObject);
	}

}