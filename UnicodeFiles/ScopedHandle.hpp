#pragma once
#include "stdafx.h"

namespace UnicodeFiles
{
	template <class T>
	class ScopedHandle
	{
	public:

		ScopedHandle()
			: mObject(GetDefaultValue())
		{
		}

		ScopedHandle(const T& aObject)
			: mObject(aObject)
		{
		}

		operator T() const
		{
			return mObject;
		}

		void Close();
		void Set(const T& aObjec);
		void Reset(const T& aObject);


		~ScopedHandle()
		{
			Close();
		}

	private:
		T GetDefaultValue();

		T mObject;
	};
}