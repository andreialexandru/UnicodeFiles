# Unicode Files


Convert files from one Unicode encoding to another (ex. Utf-8 -> Utf-16, Utf-32 -> Utf-8 etc). Currently it only works on Windows.

This is a toy project developed while at Caphyon (Advanced Installer) to get aquainted with Unicode and Doxygen.

**To Do**
 * Make it portable.
